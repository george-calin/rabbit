import React from "react";
import BaseRouter from "./routes";
import { BrowserRouter } from "react-router-dom";
import BaseLayout from "./containers/BaseLayout";
import "./style/icon_library";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends React.Component {
  render() {
    return (
      <React.StrictMode>
        <div>
          <BrowserRouter>
            <BaseLayout {...this.props}>
              <BaseRouter />
            </BaseLayout>
          </BrowserRouter>
        </div>
      </React.StrictMode>
    );
  }
}

export default App;
