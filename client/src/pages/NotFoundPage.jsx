import React from 'react';
class NotFoundPage extends React.Component{
    render(){
        return <div>
            {/* <img src={PageNotFound}  /> */}
            <h1 style={{textAlign: "center", color:'orange', fontSize:"100px"}} >
                404
            </h1>
            <h1 style={{textAlign: "center", fontSize:"80px", color:"grey"}} >
                Oops ! Page not found
            </h1>
          </div>;
    }
}
export default NotFoundPage;