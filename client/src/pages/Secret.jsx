import React from "react";
import api from "../api";
export default class Secret extends React.Component {
  constructor() {
    super();
    //Set default message
    this.state = {
      message: "Loading..."
    };
  }
  componentDidMount() {
    //GET message from server using fetch api
    api
      .checkToken()
      // fetch("/api/checkToken")
      .then(res => res.text())
      .then(res => {
        this.setState({ message: res });
      });
  }
  render() {
    return (
      <div>
        <h1>Home</h1>
        <p>{this.state.message}</p>
      </div>
    );
  }
}
