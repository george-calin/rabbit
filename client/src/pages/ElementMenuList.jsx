import React, { Component } from "react";
import ReactTable from "react-table";
import api from "../api";
import styled from "styled-components";
import "react-table/react-table.css";
import { DeleteIcon, EditIcon } from "../style/icons";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { makeStyles, withStyles } from "@material-ui/core/styles";

const Wrapper = styled.div`
  padding: 0px 40px 40px 40px;
`;

const styles = makeStyles(theme => ({
  row: {
    marginBottom: 10
  }
}));

class ElementMenusList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      elementMenus: [],
      columns: [],
      isLoading: false
    };
  }

  handleRedirect = () => {
    this.props.history.push("create");
  };

  handleUpdate = id => {
    const path = `/element_menu/update/${id}`;
    this.props.history.push(path);
  };

  handleDelete = id => {
    if (
      window.confirm(
        `Do tou want to delete the menu element ${id} permanently?`
      )
    ) {
      api.deleteElementMenuById(id);
      window.location.reload();
    }
  };

  componentDidMount = async () => {
    this.setState({ isLoading: true });

    await api.getAllElementMenus().then(elementMenus => {
      this.setState({
        elementMenus: elementMenus.data.data,
        isLoading: false
      });
    });
  };

  render() {
    const { classes } = this.props;
    const { elementMenus, isLoading } = this.state;
    const handleUpdate = this.handleUpdate;
    const handleDelete = this.handleDelete;

    console.log(
      "TCL: ElementMenusList -> render -> elementMenus",
      elementMenus
    );

    const columns = [
      {
        Header: "ID",
        accessor: "_id",
        filterable: true
      },
      {
        Header: "Title",
        accessor: "title",
        filterable: true
      },
      {
        Header: "Url",
        accessor: "url",
        filterable: true
      },
      {
        Header: "Icon Name",
        accessor: "iconName",
        filterable: true
      },
      {
        Header: "Import Path",
        accessor: "importPath",
        filterable: true
      },
      {
        Header: "Icon Variant",
        accessor: "iconVariant",
        filterable: true
      },
      {
        Header: "Update",
        accessor: "",
        Cell: function(props) {
          return (
            <IconButton
              aria-label="update"
              color="primary"
              id={props.original._id}
              onClick={e => handleUpdate(props.original._id)}
              style={{ color: "green" }}
            >
              <EditIcon />
            </IconButton>
          );
        }
      },
      {
        Header: "Delete",
        accessor: "",
        Cell: function(props) {
          return (
            <IconButton
              aria-label="delete"
              color="secondary"
              id={props.original._id}
              onClick={e => handleDelete(props.original._id)}
            >
              <DeleteIcon />
            </IconButton>
          );
        }
      }
    ];

    const showTable = elementMenus.length ? true : false;

    return (
      <Wrapper>
        <Grid
          classnames={classes.row}
          container
          direction="row"
          justify="flex-end"
          alignItems="center"
          style={{ marginBottom: 10 }}
        >
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleRedirect}
          >
            Create New
          </Button>
        </Grid>
        {showTable && (
          <ReactTable
            data={elementMenus}
            columns={columns}
            loading={isLoading}
            defaultPageSize={10}
            showPageSizeOptions={true}
            minRows={0}
          />
        )}
      </Wrapper>
    );
  }
}

export default withStyles(styles)(ElementMenusList);
