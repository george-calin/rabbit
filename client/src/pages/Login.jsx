// Login.jsx
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as actions from "../store/actions/auth";
import api from "../api";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  handleInputChange = event => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const body = JSON.stringify(this.state);

    api
      .authenticateUser(body)
      .then(res => {
        if (res.status === 200) {
          this.props.updateCredentials(res.data);
          this.props.history.push("/");
        } else {
          const error = new Error(res.error);
          throw error;
        }
      })
      .catch(err => {
        console.error(err);
        alert("Error logging in please try again");
      });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <h1>Login Below!</h1>
        <input
          type="email"
          name="email"
          placeholder="Enter email"
          value={this.state.email}
          onChange={this.handleInputChange}
          required
        />
        <input
          type="password"
          name="password"
          placeholder="Enter password"
          value={this.state.password}
          onChange={this.handleInputChange}
          required
        />
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

const mapStateToProps = store => {
  return {
    email: store.email
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCredentials: payload => dispatch(actions.updateCredentials(payload))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
