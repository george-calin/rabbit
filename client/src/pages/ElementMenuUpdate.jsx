
import React, { Component } from 'react'
import api from '../api'

import styled from 'styled-components'

const Title = styled.h1.attrs({
    className: 'h1',
})``

const Wrapper = styled.div.attrs({
    className: 'form-group',
})`
    margin: 0 30px;
`

const Label = styled.label`
    margin: 5px;
`

const InputText = styled.input.attrs({
    className: 'form-control',
})`
    margin: 5px;
`

const Button = styled.button.attrs({
    className: `btn btn-primary`,
})`
    margin: 15px 15px 15px 5px;
`

const CancelButton = styled.a.attrs({
    className: `btn btn-danger`,
})`
    margin: 15px 15px 15px 5px;
`

class ElementMenuUpdate extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            title: '',
            url: '',
            iconName: '',
            importPath: ''
        }
    }

    handleChangeTitle = async event => {
        const title = event.target.value
        this.setState({ title })
    }
    handleChangeURL = async event => {
        const url = event.target.value
        this.setState({ url })
    }
    handleChangeIconName = async event => {
        const iconName = event.target.value
        this.setState({ iconName })
    }
    handleChangeImportPath = async event => {
        const importPath = event.target.value
        this.setState({ importPath })
    }

    handleUpdateElementMenu = async () => {
        const { id, title, url, iconName, importPath } = this.state
        const payload = { title, url, iconName, importPath }
        await api.updateElementMenuById(id, payload).then(res => {
            window.alert(`ElementMenu updated successfully`)
            this.props.history.push('/element_menu/list');
        })
    }

    componentDidMount = async () => {
        const { id } = this.state
        const item = await api.getElementMenuById(id)

        this.setState({
            title: item.data.data.title,
            url: item.data.data.url,
            iconName: item.data.data.iconName,
            importPath: item.data.data.importPath
        })
    }

    render() {
        const { title, url, iconName, importPath } = this.state;
        return (
            <Wrapper>
                <Title>Update Element Menu</Title>

                <Label>Title: </Label>
                <InputText
                    type="text"
                    value={title}
                    onChange={this.handleChangeTitle}
                />
                <Label>URL: </Label>
                <InputText
                    type="text"
                    value={url}
                    onChange={this.handleChangeURL}
                />
                <Label>Icon Name: </Label>
                <InputText
                    type="text"
                    value={iconName}
                    onChange={this.handleChangeIconName}
                />
                <Label>Import Path: </Label>
                <InputText
                    type="text"
                    value={importPath}
                    onChange={this.handleChangeImportPath}
                />

                <Button onClick={this.handleUpdateElementMenu}>Update ElementMenu</Button>
                <CancelButton href={'/element_menu/list'}>Cancel</CancelButton>
            </Wrapper>
        )
    }
}

export default ElementMenuUpdate