import React, { Component } from "react";
import api from "../api";
import Select from "../components/Select";
import styled from "styled-components";
// import iconVariants from '../constants/iconVariants';
import { FormControl, Input, InputLabel } from "@material-ui/core";

const Title = styled.h1.attrs({
  className: "h1"
})``;

const Wrapper = styled.div.attrs({
  className: "form-group"
})`
  margin: 0 30px;
`;

const Button = styled.button.attrs({
  className: `btn btn-primary`
})`
  margin: 15px 15px 15px 5px;
`;

const CancelButton = styled.a.attrs({
  className: `btn btn-danger`
})`
  margin: 15px 15px 15px 5px;
`;

class ElementMenuInsert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      url: "",
      iconName: "",
      importPath: "",
      iconVariants: []
    };
  }

  async componentDidMount() {
    await api.getAllIconVariants().then(res => {
      const { data, success } = res.data;
      if (success) {
        this.setState({
          iconVariants: data
        });
      }
    });
  }

  handleChangeTitle = async event => {
    const title = event.target.value;
    this.setState({ title });
  };
  handleChangeURL = async event => {
    const url = event.target.value;
    this.setState({ url });
  };
  handleChangeIconName = async event => {
    const iconName = event.target.value;
    this.setState({ iconName });
  };
  handleChangeImportPath = async event => {
    const importPath = event.target.value;
    this.setState({ importPath });
  };
  handleChangeVariant = async event => {
    const iconVariant = event.target.value;
    this.setState({ iconVariant });
  };

  handleInsertElementMenu = async () => {
    const { title, url, iconName, importPath, iconVariant } = this.state;
    const payload = { title, url, iconName, importPath, iconVariant };

    await api.insertElementMenu(payload).then(res => {
      window.alert(`Element Menu successfully`);
      this.props.history.push("/element_menu/list");
    });
  };

  render() {
    const { iconVariants } = this.state;
    return (
      <Wrapper>
        <Title>Create Element Menu</Title>
        <FormControl margin="normal" fullWidth={true}>
          <InputLabel htmlFor="input-name">Name</InputLabel>
          <Input id="input-name" onChange={this.handleChangeTitle} />
        </FormControl>
        <FormControl margin="normal" fullWidth={true}>
          <InputLabel htmlFor="input-url">URL</InputLabel>
          <Input id="input-url" onChange={this.handleChangeURL} />
        </FormControl>
        <FormControl margin="normal" fullWidth={true}>
          <InputLabel htmlFor="input-icon_name">Icon Name</InputLabel>
          <Input id="input-icon_name" onChange={this.handleChangeIconName} />
        </FormControl>
        <FormControl margin="normal" fullWidth={true}>
          <InputLabel htmlFor="input-icon_variant">Icon Variant</InputLabel>
          <Select
            id="input-icon_variant"
            children={iconVariants}
            onChange={this.handleChangeVariant}
            placeholder="Selectati o varianta de icoana"
          />
        </FormControl>
        <FormControl margin="normal" fullWidth={true}>
          <InputLabel htmlFor="input-import_path">Import Path</InputLabel>
          <Input id="input-import_path" />
        </FormControl>
        {/* <Label>Import Path: </Label> */}
        {/* <Select 
                    children={iconVariants}
                    onChange={this.handleChangeVariant}
                    defaultValue={iconVariant}
                    placeholder="Selectati o varianta de icoana"
                    className="form-control"
                /> */}
        <Button onClick={this.handleInsertElementMenu}>Confirm</Button>
        <CancelButton href={"/element_menu/list"}>Cancel</CancelButton>
      </Wrapper>
    );
  }
}

export default ElementMenuInsert;
