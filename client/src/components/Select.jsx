import React from 'react'; 
import { MenuItem, Select as MaterialSelect } from '@material-ui/core';


const Select = (props) => {
	const { 
		children = [],
		onChange = () => { },
		defaultValue = '',
	} = props || {};

	return (
		<MaterialSelect 
			// value={defaultValue}
			value={defaultValue}
			labelId="label"
			id="select"
			onChange={onChange}
			className={props.className}
		>
		{
			children.map( (item) => (
				<MenuItem 
					key={item._id}
					value={item.value}
				>
					{item.placeholder}
				</MenuItem>
			))
		}
		</MaterialSelect>
	)
}

export default Select;