import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3000/api",
  headers: {
    "Content-Type": "application/json"
  }
});

export const insertElementMenu = payload => api.post(`/elementMenu`, payload);
export const getAllElementMenus = () => api.get(`/elementMenus`);
export const updateElementMenuById = (id, payload) =>
  api.put(`/elementMenu/${id}`, payload);
export const deleteElementMenuById = id => api.delete(`/elementMenu/${id}`);
export const getElementMenuById = id => api.get(`/elementMenu/${id}`);
export const authenticateUser = payload =>
  api.post(`/user/authenticate`, payload);
export const getAllIconVariants = () => api.get("/iconVariants");
export const checkToken = () => api.get("/checkToken");

const apis = {
  authenticateUser,
  insertElementMenu,
  getAllElementMenus,
  updateElementMenuById,
  deleteElementMenuById,
  getElementMenuById,
  getAllIconVariants,
  checkToken
};

export default apis;
