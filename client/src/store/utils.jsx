export const updateObject = (oldObject, updatedProps) => {
  return {
    ...updateObject,
    ...updatedProps
  };
};
