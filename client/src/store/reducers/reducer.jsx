import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utils";

const initialState = {
  token: null,
  email: null,
  test_var: 0
};

const testAction = (state, action) => {
  return updateObject(state, {
    test_var: action.test_var
  });
};

const updateCredentials = (state, action) => {
  return updateObject(state, action);
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TEST_ACTION:
      return testAction(state, action);
    case actionTypes.UPDATE_CREDENTIALS:
      return updateCredentials(state, action);
    default:
      return state;
  }
};

export default reducer;
