import * as actionTypes from "./actionTypes";

export const testAction = test_var => {
  return {
    type: actionTypes.TEST_ACTION,
    test_var: test_var
  };
};

export const updateCredentials = payload => {
  return {
    type: actionTypes.UPDATE_CREDENTIALS,
    email: payload.email || "",
    token: payload.token || ""
  };
};
