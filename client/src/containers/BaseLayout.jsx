import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
// import { Link } from '@material-ui/core';
import api from '../api'
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Logo from '../components/Logo';
import { Container } from '@material-ui/core';
import { ClippedDrawer } from '../components/';
// import { BottomNav } from '../components/';

const styles = theme => ({
	root: {
		display: 'flex',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3),
	},
	toolbar: theme.mixins.toolbar,
});

class BaseLayout extends Component {
	state = {
		elementMenus: []
	};
	getElementMenus = async() => {
        await api.getAllElementMenus().then(elementMenus => {
			let data = elementMenus.data.data;
            this.setState({
                elementMenus: data,
            });
        });
	}
	componentDidMount = async() => {
		await this.getElementMenus();
	}

	render () {
		const { classes } = this.props;
		const { elementMenus } = this.state


		return (
			<div className={classes.root}>
				<CssBaseline />
				<AppBar position="fixed" className={classes.appBar}>
					<Toolbar>
						<Logo />
						<Typography variant="h6" noWrap>
							Clipped drawer
						</Typography>
					</Toolbar>
				</AppBar>
				<ClippedDrawer data={elementMenus}/>
				<Container className={classes.content} >
					<div className={classes.toolbar} />
					{this.props.children}
				</Container>
			</div>
		);
	}
}

BaseLayout.propTypes = {
	classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(BaseLayout);




