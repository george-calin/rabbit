import { library } from '@fortawesome/fontawesome-svg-core';

import {
	faGithubAlt,
	faGoogle,
	faFacebook,
	faTwitter
} from '@fortawesome/free-brands-svg-icons';

import { 
	faCheckSquare,
	faCoffee,
	faHome 
} from '@fortawesome/free-solid-svg-icons';

library.add(
	faGithubAlt,
	faGoogle,
	faFacebook,
	faTwitter,
	faCheckSquare,
	faCoffee,
	faHome
);

export default library