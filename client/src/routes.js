import React from "react";
import { Route, Switch } from "react-router-dom";
import {
  ElementMenuInsert,
  ElementMenuList,
  ElementMenuUpdate,
  NotFoundPage
} from "./pages";
import Secret from "./pages/Secret";
import Login from "./pages/Login";
import withAuth from "./hocs/withAuth";

const BaseRouter = () => (
  <div>
    <Switch>
      <Route path="/element_menu/list" exact component={ElementMenuList} />
      <Route path="/element_menu/create" exact component={ElementMenuInsert} />
      <Route
        path="/element_menu/update/:id"
        exact
        component={ElementMenuUpdate}
      />
      <Route path="/secret" exact component={withAuth(Secret)} />
      <Route path="/login" exact component={Login} />
      <Route component={NotFoundPage} />
    </Switch>
  </div>
);

export default BaseRouter;
