import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import FormatListBulletedIcon from "@material-ui/icons/FormatListBulleted";
// import AddCircleOutlineIcon from '@mterial-ui/icons/AddCircleOutline';

const pageLinks = [
  {
    title: "Home",
    url: "/",
    icon: <HomeIcon />
  },
  {
    title: "Element Menu List",
    url: "/element_menu/list",
    icon: <FormatListBulletedIcon />
  },
  {
    title: "Login",
    url: "/login",
    icon: <HomeIcon />
  },
  {
    title: "Secret",
    url: "/secret",
    icon: <HomeIcon />
  }
];

export default pageLinks;
