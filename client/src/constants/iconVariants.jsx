const iconVariants = [
	{
		id: '1',
		name: 'fas',
	},
	{
		id: '2',
		name: 'fab',
	},
]

export default iconVariants;