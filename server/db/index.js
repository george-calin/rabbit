const mongoose = require(`mongoose`);
const connUri = process.env.MONGO_LOCAL_CONN_URL;

mongoose
  .connect(connUri, {
    useUnifiedTopology: true, // gives warning
    useNewUrlParser: true
  })
  .catch(e => {
    console.error(`Connection error`, e.message);
  });

const db = mongoose.connection;

module.exports = db;
