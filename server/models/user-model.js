const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Schema = mongoose.Schema;

const environment = process.env.NODE_ENV; // development
const stage = require("../config")[environment];

// const saltRounds = 10;

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, trim: true, unique: true },
  password: { type: String, required: true, trim: true }
});

UserSchema.pre("save", function(next) {
  const user = this;
  // don't rehash if it's an old user
  if (!user.isModified("password") && !user.isNew) {
    next();
  } else {
    bcrypt.hash(user.password, stage.saltingRounds, function(err, hash) {
      if (err) {
        console.log("Error hasing password for user", user.name);
        next(err);
      } else {
        user.password = hash;
        next();
      }
    });
  }
});

UserSchema.methods.isCorrectPassword = function(password, cb) {
  bcrypt.compare(password, this.password, function(err, match) {
    if (err) {
      cb(err);
    } else {
      cb(err, match);
    }
  });
};

module.exports = mongoose.model("User", UserSchema);
