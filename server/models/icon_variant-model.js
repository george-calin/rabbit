const mongoose = require('mongoose')
const Schema = mongoose.Schema

const IconVariant = new Schema(
    {
        value:        	{ type: String,     required: true},
        placeholder:    { type: String,     required: true},
    },
)

module.exports = mongoose.model('IconVariant', IconVariant)