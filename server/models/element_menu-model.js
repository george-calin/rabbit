const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ElementMenu = new Schema(
    {
        title:          { type: String,     required: true},
        url:            { type: String,     required: true},
        iconName:       { type: String,     required: false},
        importPath:     { type: String,     required: false},
        iconVariant:    { type: String,     required: true, default: "fas"},
    },
)

module.exports = mongoose.model('ElementMenu', ElementMenu)