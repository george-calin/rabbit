require("dotenv").config(); // Sets up dotenv as soon as our application starts

const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");

const db = require("./db");
const elementMenuRouter = require("./routes/element_menu-router");
const userRouter = require("./routes/user-router");
const authRouter = require("./routes/auth-router");
const iconVariantRouter = require("./routes/icon_variant-router");

const app = express();
// const apiPort = 3000

const environment = process.env.NODE_ENV; // development
const stage = require("./config")[environment];

app.use("/", express.static(__dirname + "/"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(cookieParser());

if (environment !== "production") {
  app.use(logger("dev"));
}

db.on("error", console.error.bind(console, "MongoDB connection error:"));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.use("/api", elementMenuRouter);
app.use("/api", iconVariantRouter);
app.use("/api/user", userRouter);
app.use("/api", authRouter);

app.listen(`${stage.port}`, () => {
  console.log(`Server now listening at localhost:${stage.port}`);
});
