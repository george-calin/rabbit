const express = require(`express`)

const ElementMenuCtrl = require(`../controllers/element_menu-ctrl`)

const router = express.Router()

router.post(`/elementMenu`, ElementMenuCtrl.createElementMenu)
router.put(`/elementMenu/:id`, ElementMenuCtrl.updateElementMenu)
router.delete(`/elementMenu/:id`, ElementMenuCtrl.deleteElementMenu)
router.get(`/elementMenu/:id`, ElementMenuCtrl.getElementMenuById)
router.get(`/elementMenus`, ElementMenuCtrl.getElementMenus)

module.exports = router