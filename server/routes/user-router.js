const express = require("express");

const UserCtrl = require("../controllers/user-ctrl");

const router = express.Router();

router.post("/register", UserCtrl.registerUser);
router.post("/authenticate", UserCtrl.authUser);

module.exports = router;
