const express = require("express");

const AuthCtrl = require("../controllers/auth-ctrl");

const router = express.Router();

// router.get("/secret", AuthCtrl.withAuth, function(req, res) {
//   console.log("SECRET ENTERED");
//   res.send("The password is potato");
// });

router.get("/checkToken", AuthCtrl.withAuth, function(req, res) {
  console.log("CHECK TOKEN ENTERED");
  res.sendStatus(200);
});

module.exports = router;
