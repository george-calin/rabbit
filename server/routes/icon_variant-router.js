const express = require(`express`)

const IconVariantCtrl = require(`../controllers/icon_variant-ctrl`)

const router = express.Router()

router.get(`/iconVariant/:id`, IconVariantCtrl.getIconVariantById)
router.get(`/iconVariants`, IconVariantCtrl.getIconVariants)

module.exports = router