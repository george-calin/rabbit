const IconVariant = require("../models/icon_variant-model");

getIconVariantById = async (req, res) => {
  await IconVariant.findOne({ _id: req.params.id }, (err, iconVariant) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }

    if (!iconVariant) {
      return res
        .status(404)
        .json({ success: false, error: `IconVariant not found` });
    }
    return res.status(200).json({ success: true, data: iconVariant });
  }).catch(err => console.log(err));
};

getIconVariants = async (req, res) => {
  await IconVariant.find({}, (err, iconVariants) => {
    if (err) {
      return res.status(400).json({ success: false, error: err });
    }
    return res.status(200).json({ success: true, data: iconVariants });
  }).catch(err => console.log(err));
};

module.exports = {
  getIconVariants,
  getIconVariantById
};
