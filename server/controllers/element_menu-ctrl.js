
const ElementMenu = require('../models/element_menu-model')

createElementMenu = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a elementMenu',
        })
    }

    const elementMenu = new ElementMenu(body)

    if (!elementMenu) {
        return res.status(400).json({ success: false, error: err })
    }

    elementMenu
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: elementMenu._id,
                message: 'ElementMenu created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'ElementMenu not created!',
            })
        })
}

updateElementMenu = async (req, res) => {
    
    const body = req.body
    console.log(body);
    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    };

    ElementMenu.findOne({ _id: req.params.id }, (err, elementMenu) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'ElementMenu not found!',
            })
        };
        elementMenu.title = body.title;
        elementMenu.url = body.url;
        elementMenu.iconName = body.iconName;
        elementMenu.importPath = body.importPath;
        elementMenu
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: elementMenu._id,
                    message: 'ElementMenu updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'ElementMenu not updated!',
                })
            })
    })
}

deleteElementMenu = async (req, res) => {
    await ElementMenu.findOneAndDelete({ _id: req.params.id }, (err, elementMenu) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!elementMenu) {
            return res
                .status(404)
                .json({ success: false, error: `ElementMenu not found` })
        }

        return res.status(200).json({ success: true, data: elementMenu })
    }).catch(err => console.log(err))
}

getElementMenuById = async (req, res) => {
    await ElementMenu.findOne({ _id: req.params.id }, (err, elementMenu) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!elementMenu) {
            return res
                .status(404)
                .json({ success: false, error: `ElementMenu not found` })
        }
        return res.status(200).json({ success: true, data: elementMenu })
    }).catch(err => console.log(err))
}

getElementMenus = async (req, res) => {
    await ElementMenu.find({}, (err, elementMenus) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        return res.status(200).json({ success: true, data: elementMenus })
    }).catch(err => console.log(err))
}

module.exports = {
    createElementMenu,
    updateElementMenu,
    deleteElementMenu,
    getElementMenus,
    getElementMenuById,
}