// const secret = "mysecretsshhh";
const jwt = require("jsonwebtoken");
const secret = process.env.JWT_SECRET;
// Import our user schema
const User = require("../models/user-model");

// POST route to register a user
const registerUser = (req, res) => {
  const { email, password } = req.body;
  const user = new User({ email, password });
  user.save(function(err) {
    console.log(err);
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(user);
    }
  });
};

const authUser = (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email }, (err, user) => {
    if (err) {
      console.error(err);
      req.status(500).json({
        error: "Internal error please try again"
      });
    } else if (!user) {
      console.log("Incorrect email");
      res.status(401).json({
        error: "Incorrect email or password"
      });
    } else {
      user.isCorrectPassword(password, (err, match) => {
        if (err) {
          res.status(500).json({
            error: "Internal error, please try again later"
          });
        } else if (!match) {
          console.log("Incorrect password");
          res.status(401).json({
            error: "Incorrect email or password"
          });
        } else {
          // Issue token
          const payload = { email };
          const options = { expiresIn: "2d" };
          const token = jwt.sign(payload, secret, options);
          const result = { email, token };
          // res.cookie("token", token, { httpOnly: true }).sendStatus(200);
          res.status(200).send(result);
        }
      });
    }
  });
};

module.exports = {
  registerUser,
  authUser
};
